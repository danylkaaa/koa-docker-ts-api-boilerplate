import { TransformClassToPlain } from 'class-transformer';
import { Body, Delete, Get, JsonController, Post } from 'routing-controllers';
import { Inject } from 'typedi';
import { ImagePresetModel } from '../../database/entities/ImagePresets/ImagePreset.model';
import { ImagesService } from '../../services/Images.service';
import { CreateImagePresetApiDto } from './CreateImagePresetApi.dto';

@JsonController('/image-presets')
export class ImagesPresetsController {
    @Inject()
    private imagePresetsService: ImagesService;

    @Post('/')
    public async createImagePreset(@Body() data: CreateImagePresetApiDto): Promise<object> {
        return (await this.imagePresetsService.createAsset(data)).getPlain();
    }

    @Delete('/jobs')
    public async deleteImageGenerationJobs(): Promise<string> {
        await this.imagePresetsService.jobsService.deleteAllJobsGenerateImage();
        return 'deleted';
    }

    @Get('/:id')
    @TransformClassToPlain()
    public async getImage(@Body({ required: true }) data: CreateImagePresetApiDto): Promise<ImagePresetModel> {
        return this.imagePresetsService.createAsset(data);
    }
}
