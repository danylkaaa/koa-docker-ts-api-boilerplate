import { JsonController, Get } from 'routing-controllers';

@JsonController()
export class RootController {
    @Get('/health-check')
    getAll(): string {
        return 'alive';
    }
}
