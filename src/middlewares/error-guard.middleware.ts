import { Middleware, KoaMiddlewareInterface, BadRequestError } from 'routing-controllers';
import { InternalServerError } from '../errors/InternalServerError';
import { ValidationError } from '../errors/ValidationError';
import { getLogger } from '../shared/Logger';
import { ServerDefaultContext } from '../typings';
import { wrapError, wrapNotFound } from '../shared/responseWrappers';
import { ApiError } from '../errors/ApiError';

const logger = getLogger('error-guard');

@Middleware({ type: 'before' })
export class ErrorGuardMiddleware implements KoaMiddlewareInterface {
    async use(ctx: ServerDefaultContext, next: () => any): Promise<void> {
        try {
            await next();
            // Respond 404 Not Found for unhandled request
            if (!ctx.body && (!ctx.status || ctx.status === 404)) {
                wrapNotFound(ctx);
            }
        } catch (err) {
            if (err instanceof BadRequestError) {
                wrapError(ctx, ValidationError.fromBadRequestError(err as any).toJSON());
            } else if (err instanceof ApiError) {
                wrapError(ctx, (err as ApiError).toJSON());
            } else {
                logger.error('InternalServerError: %s', err.toString());
                wrapError(ctx, InternalServerError.fromRuntimeError(err, ctx.isDev).toJSON());
                // retaining the default behaviour in Koa
                // Recommended for centralized error reporting,
                ctx.app.emit('error', err, ctx);
            }
        }
    }
}
