import { MongoClient, MongoClientOptions } from 'mongodb';
import { getLogger } from '../shared/Logger';

const logger = getLogger('mongo');

export const connect = async (address: string, options?: MongoClientOptions): Promise<MongoClient> => {
    logger.info('establish connection....');
    try {
        const client = await MongoClient.connect(address, {
            ...options,
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });

        logger.info('connected');

        return client;
    } catch (e) {
        logger.error('unable to connect: %s', e.message);
        throw e;
    }
};

export const close = async (client: MongoClient): Promise<void> => {
    logger.info('closing connection....');
    await client.close();
    logger.info('connection closed');
};
