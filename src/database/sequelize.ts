import { Sequelize, SequelizeOptions } from 'sequelize-typescript';
import { getLogger } from '../shared/Logger';

const logger = getLogger('sequelize');

export const connect = async (config: SequelizeOptions): Promise<Sequelize> => {
    logger.info('establish connection....');
    try {
        const sequelize = new Sequelize({
            ...config,
            models: [`${__dirname}/models/**/*.model.ts`],
        });

        await sequelize.authenticate();
        logger.info('connected');

        return sequelize;
    } catch (e) {
        logger.error('unable to connect: %s', e.message);
        throw e;
    }
};

export const close = async (sequelize: Sequelize): Promise<void> => {
    logger.info('closing connection....');
    await sequelize.close();
    logger.info('connection closed');
};
