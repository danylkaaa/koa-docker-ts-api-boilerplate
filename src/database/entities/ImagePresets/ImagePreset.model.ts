import { classToPlain, Expose, plainToClass } from 'class-transformer';
import { IsString } from 'class-validator';
import { FractalTypes } from '../../../constants';

@Expose()
export class ImagePresetModel {
    @Expose()
    public readonly id: string;

    @Expose()
    @IsString()
    public readonly type: FractalTypes;

    @Expose()
    public readonly width: number;

    @Expose()
    public readonly height: number;

    @Expose()
    public readonly centerX: number | null;

    @Expose()
    public readonly centerY: number | null;

    @Expose()
    public readonly maxZ: number | null;

    @Expose()
    public readonly color: string | null;

    @Expose()
    public readonly power: number | null;

    @Expose()
    public readonly iterations: number | null;

    public static fromMongoDoc(doc: any): ImagePresetModel {
        return plainToClass(ImagePresetModel, { ...doc, id: doc._id.toString() }, { excludeExtraneousValues: true });
    }

    public getPlain(): object {
        return classToPlain(this);
    }
}
