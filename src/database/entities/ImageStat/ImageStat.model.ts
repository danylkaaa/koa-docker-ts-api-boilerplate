import { classToPlain, Expose, plainToClass } from 'class-transformer';

@Expose()
export class ImageStatModel {
    @Expose()
    public readonly id: string;

    @Expose()
    public readonly presetId: string;

    @Expose()
    public readonly generationTime: number;

    @Expose()
    public readonly settings: object;

    public static fromMongoDoc(doc: any): ImageStatModel {
        return plainToClass(ImageStatModel, { ...doc, id: doc._id.toString() }, { excludeExtraneousValues: true });
    }

    public getPlain(): object {
        return classToPlain(this);
    }
}
