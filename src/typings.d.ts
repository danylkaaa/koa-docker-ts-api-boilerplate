import Koa from 'koa';
import { FractalTypes } from './constants';

export interface ServerDefaultContext extends Koa.Context {
    doNotWrapResponse: boolean;
    isDev: boolean;
    isTest: boolean;
}

export interface ServerResponse {
    data?: {};
    message?: string;
}

export interface ServerErrorResponse extends ServerResponse {
    status: number;
    fault: string;
    stack?: string;
}

export interface ImageConfiguration {
    width: number;
    height: number;
    type: FractalTypes;
    centerX: number;
    centerY: number;
    maxZ: number;
    color: string;
    power: number;
    iterations: number;
    presetId: string;
}
