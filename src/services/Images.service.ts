import { MongoClient } from 'mongodb';
import { Inject, Service } from 'typedi';
import { FractalTypes, PRE_GENERATED_IMAGES_COUNT } from '../constants';
import { ImagePresetModel } from '../database/entities/ImagePresets/ImagePreset.model';
import ImagePresetsRepository from '../database/entities/ImagePresets/ImagePresets.repository';
import { generateImageSettings } from '../shared/randomDataGenerators/ImageSettingsGenerator';
import { ImageConfiguration } from '../typings';
import { JobsCreatorService } from './JobsCreator.service';

export interface CreateImagePresetDto {
    type: FractalTypes;
    width: number;
    height: number;
    centerX: number | null;
    centerY: number | null;
    maxZ: number | null;
    color: string | null;
    power: number | null;
    iterations: number | null;
}
@Service()
export class ImagesService {
    @Inject()
    readonly mongo: MongoClient;

    @Inject()
    readonly imagePresetsRepository: ImagePresetsRepository;

    @Inject()
    readonly jobsService: JobsCreatorService;

    public randomizeImagePreset(
        imagePreset: ImagePresetModel,
        count = PRE_GENERATED_IMAGES_COUNT,
    ): ImageConfiguration[] {
        return generateImageSettings(imagePreset, count);
    }

    public async createAsset(data: CreateImagePresetDto): Promise<ImagePresetModel> {
        const { insertedId } = await this.imagePresetsRepository.collection.insertOne(data);

        const imagePreset = ImagePresetModel.fromMongoDoc(
            await this.imagePresetsRepository.collection.findOne({ _id: insertedId }),
        );

        // generate N random image based on this configuration
        // const randomImages = this.randomizeImagePreset(imagePreset);

        // // create job to generate images based on this configuration asynchronously
        // await Promise.all(
        //     randomImages.map((imageConfiguration: ImageConfiguration) =>
        //         this.jobsService.createJobGenerateImage({ imageConfiguration }),
        //     ),
        // );

        return imagePreset;
    }
}
