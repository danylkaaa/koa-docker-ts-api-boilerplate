import { MongoClient } from 'mongodb';
import { Inject, Service } from 'typedi';
import { ImageStatModel } from '../database/entities/ImageStat/ImageStat.model';
import ImageStatRepository from '../database/entities/ImageStat/ImageStat.repository';
import { ImageConfiguration } from '../typings';

export interface CreateImageStatDto {
    preset: ImageConfiguration;
    id: string;
    executionTime: number;
}

@Service()
export class ImagesStatsService {
    @Inject()
    readonly mongo: MongoClient;

    @Inject()
    readonly imageStatRepository: ImageStatRepository;

    public async createStat(data: CreateImageStatDto): Promise<string> {
        const { insertedId } = await this.imageStatRepository.collection.insertOne(data);

        return insertedId.toString();
    }

    public async updateImageGenerationTime(id: string, delay: number): Promise<ImageStatModel> {
        const { value } = await this.imageStatRepository.collection.findOneAndUpdate(
            { _id: id },
            { generationTime: delay },
        );

        return ImageStatModel.fromMongoDoc(value);
    }
}
