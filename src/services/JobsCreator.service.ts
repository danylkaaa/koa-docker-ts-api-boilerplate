import Bull from 'bull';
import { Redis } from 'ioredis';
import { Inject, Service } from 'typedi';
import { Instance, JobsQueue } from '../constants';
import { getLogger } from '../shared/Logger';
import { ImageConfiguration } from '../typings';

const logger = getLogger('JobsCreatorService');

export interface CreateJobGenerateImagePayload {
    imageConfiguration: ImageConfiguration;
}

@Service()
export class JobsCreatorService {
    private readonly generateImageQueue: Bull.Queue<ImageConfiguration>;

    constructor(@Inject(Instance.REDIS) protected readonly redis: Redis) {
        this.generateImageQueue = new Bull<ImageConfiguration>(JobsQueue.GENERATE_IMAGE, {
            createClient: (): Redis => redis,
        });
    }

    getNameForGenerateImageJob({ presetId }: { presetId: string }): string {
        return `gen-image-for-${presetId}`;
    }

    private logJobCreated(queue: JobsQueue, jobId: string, data: {}): void {
        logger.info('create job in queue [%s] with jobId [%s] and payload %j', queue, jobId, data);
    }

    private logTryToDeleteAllJobs(queue: JobsQueue): void {
        logger.info('trying to delete all jobs in a queue %s', queue);
    }

    private logAllJobsDeleted(queue: JobsQueue): void {
        logger.info('all jobs in a queue %s deleted', queue);
    }

    logDeletingJobsFailed(queue: JobsQueue, reason: Error): void {
        logger.error('unable to delete all jobs queue %s because: %s', queue, reason.message);
    }

    logJobNotCreated(queue: JobsQueue, jobId: string, data: {}, reason: Error): void {
        logger.error(
            'unable job in queue [%s] with jobId [%s] and payload %s because: %s',
            queue,
            jobId,
            data,
            reason.message,
        );
    }

    async createJobGenerateImage({ imageConfiguration }: CreateJobGenerateImagePayload): Promise<void> {
        const jobId = this.getNameForGenerateImageJob(imageConfiguration);

        try {
            await this.generateImageQueue.add(jobId, imageConfiguration, {
                attempts: Infinity,
                removeOnFail: false,
            });
        } catch (e) {
            this.logJobNotCreated(JobsQueue.GENERATE_IMAGE, jobId, imageConfiguration, e);
            return null;
        }

        this.logJobCreated(JobsQueue.GENERATE_IMAGE, jobId, imageConfiguration);
    }

    async deleteAllJobsGenerateImage(): Promise<void> {
        this.logTryToDeleteAllJobs(JobsQueue.GENERATE_IMAGE);
        try {
            await this.generateImageQueue.empty();
            this.logAllJobsDeleted(JobsQueue.GENERATE_IMAGE);
        } catch (e) {
            this.logDeletingJobsFailed(JobsQueue.GENERATE_IMAGE, e);
            throw e;
        }
    }

    async close(): Promise<void> {
        await this.generateImageQueue.close(false);
    }
}
