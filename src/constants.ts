import * as path from 'path';

export enum Instance {
    CONFIG = 'CONFIG',
    SEQUELIZE = 'SEQUELIZE',
    MONGO = 'MONGO',
    REDIS = 'REDIS',
}

export enum ServerEvents {
    LISTEN = 'LISTEN',
    CLOSE = 'CLOSE',
}

export enum Environment {
    DEV = 'development',
    PRODUCTION = 'production',
    TEST = 'test',
}

export enum Tables {
    IMAGES = 'images',
}

export enum FractalTypes {
    MANDELBROT = 'mandelbrot',
    JULIA = 'julia',
}

export enum JobsQueue {
    GENERATE_IMAGE = 'generate_image',
}

export const PRE_GENERATED_IMAGES_COUNT = 1;
export const PYTHON_EXECUTABLE_FILE = 'fractal_generator.py';
export const PYTHON_GENERATOR_SCRIPT_PATH = path.join(__dirname, '../fractal-generator-master');
