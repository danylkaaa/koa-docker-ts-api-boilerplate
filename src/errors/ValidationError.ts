import _reduce from 'lodash/reduce';
import { ApiError } from './ApiError';

interface ValidationErrorDescription {
    property: string;
    value: any;
    target: object;
    children: ValidationErrorDescription[];
    constraints: object;
}

export class ValidationError extends ApiError {
    constructor(validationErrors: object) {
        super({
            message: 'Bad arguments, check data for details',
            status: 400,
            data: validationErrors,
        });
        Object.setPrototypeOf(this, ApiError.prototype);
    }

    protected static validationErrorsToHumanReadable(errors: ValidationErrorDescription[], path = ''): object {
        return _reduce(
            errors,
            (mem, error) => {
                const currentPath = `${path}${path ? '.' : ''}${error.property}`;

                mem[currentPath] = Object.values(error.constraints);

                return {
                    ...mem,
                    ...(error.children
                        ? ValidationError.validationErrorsToHumanReadable(error.children, currentPath)
                        : null),
                };
            },
            {},
        );
    }

    public static fromBadRequestError({ errors }: { errors: ValidationErrorDescription[] }): ValidationError {
        return new ValidationError(ValidationError.validationErrorsToHumanReadable(errors));
    }
}
