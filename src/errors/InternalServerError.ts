import { ApiError } from './ApiError';

export class InternalServerError extends ApiError {
    constructor(error: Error, isDev: boolean) {
        super({
            status: 500,
            originalStack: error.stack,
            message: isDev ? error.message : null,
            isDev,
        });
        Object.setPrototypeOf(this, ApiError.prototype);
    }

    public static fromRuntimeError(error: Error, isDev?: boolean): InternalServerError {
        return new InternalServerError(error, isDev);
    }
}
