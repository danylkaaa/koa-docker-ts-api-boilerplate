import * as httpStatus from 'http-status';
import { HttpError } from 'routing-controllers';
import { ServerErrorResponse } from '../typings';

interface ApiErrorConstructorOptions {
    status: number;
    message?: string;
    fault?: string;
    data?: any;
    originalStack?: string;
    isDev?: boolean;
}

export class ApiError extends HttpError {
    public readonly status: number;

    public readonly fault: string;

    public readonly data: string;

    public readonly isDev: boolean;

    constructor({ status, message, fault, data, originalStack, isDev }: ApiErrorConstructorOptions) {
        super(status);
        if (!fault) {
            fault = httpStatus[`${status}_NAME`];
        }

        this.status = status;
        this.message = message;
        this.fault = fault;
        this.data = data;
        this.isDev = !!isDev;

        if (originalStack) {
            this.stack = originalStack;
        } else {
            Error.captureStackTrace(this, this.constructor);
        }
    }

    public toJSON(): ServerErrorResponse {
        const response: ServerErrorResponse = {
            status: this.status,
            message: this.message,
            fault: this.fault,
            data: this.data,
        };

        if (this.isDev) {
            response.stack = this.stack;
        }

        return response;
    }
}
