import * as log4js from 'log4js';
import { Environment } from '../constants';
import { isTest } from './utils/common';

export const loggerConfig: log4js.Configuration = {
    appenders: {
        console: {
            type: 'console',
            layout: {
                type: 'coloured',
            },
        },
    },
    categories: {
        default: {
            appenders: ['console'],
            level: 'DEBUG',
        },
    },
};

export const getLoggerConfig = ({ env }: { env: Environment }): log4js.Configuration => {
    if (isTest(env)) {
        return { appenders: {}, categories: {} };
    }

    return loggerConfig;
};
export const init = ({ env }: { env: Environment }): void => {
    log4js.configure(getLoggerConfig({ env }));
};

export const getLogger = (label: string): log4js.Logger => log4js.getLogger(label);
