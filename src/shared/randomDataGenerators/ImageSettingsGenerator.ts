import _reduce from 'lodash/reduce';
import { ImagePresetModel } from '../../database/entities/ImagePresets/ImagePreset.model';
import config from '../../config/server';
import { ImageConfiguration } from '../../typings';

const valueBetweenGenerator = (min: number, max: number, round = 4): (() => number) => () =>
    +Number((min + Math.random() * (max - min)).toFixed(round));

const colorGenerator = (): (() => string) => () =>
    `#${Array.from({ length: 6 })
        .map(() => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f'][~~(Math.random() * 16)])
        .join('')}`;

const generationOptions = {
    presetId: (v: { id: string }): string => v.id,
    centerX: valueBetweenGenerator(config.fractalsOptions.minCenterX, config.fractalsOptions.maxCenterX),
    centerY: valueBetweenGenerator(config.fractalsOptions.minCenterY, config.fractalsOptions.maxCenterY),
    maxZ: valueBetweenGenerator(config.fractalsOptions.minZ, config.fractalsOptions.maxZ),
    color: colorGenerator(),
    power: valueBetweenGenerator(config.fractalsOptions.minPower, config.fractalsOptions.maxPower),
    iterations: valueBetweenGenerator(config.fractalsOptions.minIterations, config.fractalsOptions.maxIterations),
};

const fields = [...Object.keys(generationOptions), 'width', 'height', 'type'];

const iterateOver = <T>(initialData: object): T =>
    _reduce(
        fields,
        (mem, key) => {
            if (!initialData[key]) {
                mem[key] = generationOptions[key](initialData);
            } else {
                mem[key] = initialData[key];
            }

            return mem;
        },
        {},
    ) as any;

export const generateImageSettings = (initialData: ImagePresetModel, length: number): ImageConfiguration[] =>
    Array.from({ length }, () => iterateOver<ImageConfiguration>(initialData));
