import * as httpStatus from 'http-status';
import { ServerDefaultContext } from '../typings';

interface ResponseWrapperOptions {
    status?: number; // HTTP status code
    data?: {};
    message?: string; // description of the result
    fault?: string; // Error name
    stack?: string; // stack of error, only for dev
}

type ResponseWrapper = (ctx: ServerDefaultContext, options?: ResponseWrapperOptions) => void;

export const wrapSuccess: ResponseWrapper = (
    ctx: ServerDefaultContext,
    { status = 200, data = null, message = null } = {},
) => {
    if (!!status && status < 400) {
        ctx.status = status;
    } else if (!(ctx.status < 400)) {
        ctx.status = httpStatus.OK;
    }

    ctx.body = {
        data,
        message,
    };
};

export const wrapError: ResponseWrapper = (
    ctx: ServerDefaultContext,
    { status, fault = null, data = null, message = null, stack = null } = {},
) => {
    if (!!status && status >= 400 && status < 600) {
        ctx.status = status;
    } else if (!(ctx.status >= 500 && ctx.status < 600)) {
        ctx.status = httpStatus.INTERNAL_SERVER_ERROR;
    }

    if (!fault) {
        fault = httpStatus[`${ctx.status}_NAME`];
    }

    ctx.body = {
        status,
        data,
        message,
        fault,
        stack: stack ? stack.split('\n') : undefined,
    };
};

export const wrapNoContent: ResponseWrapper = (ctx: ServerDefaultContext, options) =>
    wrapSuccess(ctx, {
        ...options,
        status: httpStatus.NO_CONTENT,
    });

export const wrapBadRequest: ResponseWrapper = (ctx: ServerDefaultContext, options = {}) =>
    wrapError(ctx, {
        ...options,
        status: httpStatus.BAD_REQUEST,
    });

export const wrapUnauthorized: ResponseWrapper = (ctx: ServerDefaultContext, params = {}) =>
    wrapError(ctx, {
        ...params,
        status: httpStatus.UNAUTHORIZED,
    });

export const wrapForbidden: ResponseWrapper = (ctx: ServerDefaultContext, params = {}) =>
    wrapError(ctx, {
        ...params,
        status: httpStatus.FORBIDDEN,
    });

export const wrapNotFound: ResponseWrapper = (ctx: ServerDefaultContext, params = {}) =>
    wrapError(ctx, {
        ...params,
        status: httpStatus.NOT_FOUND,
    });

export const wrapInternalServerError: ResponseWrapper = (ctx: ServerDefaultContext, params = {}) =>
    wrapError(ctx, {
        ...params,
        status: httpStatus.INTERNAL_SERVER_ERROR,
    });

export const wrapNotImplemented: ResponseWrapper = (ctx: ServerDefaultContext, params = {}) =>
    wrapError(ctx, {
        ...params,
        status: httpStatus.NOT_IMPLEMENTED,
    });

export const wrapBadGateway: ResponseWrapper = (ctx: ServerDefaultContext, params = {}) =>
    wrapError(ctx, {
        ...params,
        status: httpStatus.BAD_GATEWAY,
    });
