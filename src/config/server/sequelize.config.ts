import { Options } from 'sequelize';
import config from './index';

export const development: Options = config.db;
export const production: Options = config.db;
export const test: Options = config.db;
