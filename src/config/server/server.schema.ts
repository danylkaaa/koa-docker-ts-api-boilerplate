import * as Joi from '@hapi/joi';
import { Dialect } from 'sequelize';
import { Environment } from '../../constants';

export interface ServerConfig {
    env: Environment;
    port: number;
    memcached: {
        port: number;
        host: string;
    };
    mongo: {
        uri: string;
        options: object;
    };
    db: {
        port: number;
        username: string;
        password: string;
        host: string;
        database: string;
        dialect: Dialect;
        logging: boolean;
        pool?: {
            min: number;
            max: number;
        };
    };
    redis: {
        host: string;
        port: number;
    };
    fractalsOptions: {
        maxZ: number;
        minZ: number;
        maxCenterX: number;
        minCenterX: number;
        maxCenterY: number;
        minCenterY: number;
        minPower: number;
        maxPower: number;
        maxIterations: number;
        minIterations: number;
    };
}

export const configSchema = Joi.object({
    env: Joi.string().valid(Environment.TEST, Environment.DEV, Environment.PRODUCTION),
    port: Joi.number().positive(),
    memcached: Joi.object({
        port: Joi.number().positive(),
        host: Joi.string(),
    }),
    mongo: Joi.object({
        uri: Joi.string(),
        options: Joi.object().optional(),
    }),
    db: Joi.object({
        port: Joi.number(),
        username: Joi.string(),
        password: Joi.string().allow(''),
        host: Joi.string(),
        database: Joi.string(),
        dialect: Joi.string(),
        pool: Joi.object({
            min: Joi.number().positive(),
            max: Joi.number().positive(),
        }).optional(),
        logging: Joi.boolean()
            .default(false)
            .optional(),
    }),
    redis: Joi.object({
        host: Joi.string(),
        port: Joi.number(),
    }),
    fractalsOptions: Joi.object({
        maxZ: Joi.number(),
        minZ: Joi.number(),
        maxCenterX: Joi.number(),
        minCenterX: Joi.number(),
        maxCenterY: Joi.number(),
        minCenterY: Joi.number(),
        minPower: Joi.number(),
        maxPower: Joi.number(),
        maxIterations: Joi.number(),
        minIterations: Joi.number(),
    }),
});
