const { spawn } = require('child_process');
const path = require('path');
const PYTHON_GENERATOR_SCRIPT_PATH = path.join(__dirname, './fractal-generator-master');

const args = [
    '-f',
    'mandelbrot',
    '-a',
    500,
    '-b',
    500,
    '-x',
    -4,
    '-y',
    '0',
    '-z',
    14.0,
    '-p',
    '2',
    '-c',
    '#e5884f',
    '-o',
    'my_mandelbrot.png',
].map((v) => String(v));


const child = spawn('env/bin/python3', args, {
    cwd: PYTHON_GENERATOR_SCRIPT_PATH,
});

child.stdout.on('data', (data) => {
    console.log('Child data: ' + data);
});
child.on('error', (e) => {
    console.log('Failed to start child.', e);
});
child.on('close', (code) => {
    console.log('Child process exited with code ' + code);
});
child.stdout.on('end', () => {
    console.log('Finished collecting data chunks.');
});
